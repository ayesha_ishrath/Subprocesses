import  subprocess
from subprocess import call
import json
from subprocess import Popen, PIPE

p = Popen(['lsblk', '/dev/sda'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
output, err = p.communicate()
data_list = []
for drives in output.split("\n"):
    try:
        details = drives.split(" ")
        details = filter(None, details)
        data = {"name":details[0][-4:],"space":details[3]}
        data_list.append(data)
    except Exception as e:
        print e
print json.dumps(data_list)

f = Popen(['free','/home'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
output, err = f.communicate()
free_space=[]
for space in output.split("\n"):
    try:
        space = space.split(" ")
        space = filter(None,space)
        mem = {"total":space[0],"used":space[1],"free":space[2],"shared":space[3],"buffered":space[4],"cached":space[5]}
        free_space.append(mem)
    except Exception as d:
        print d
print json.dumps(free_space)
